---
title: ARCHONS Code of Conduct
icon: material/circle
---

The ARCHONS project is committed to fostering a welcoming, inclusive, and collaborative environment for all contributors and users. We value diversity, respect, and open communication, and we expect all members of our community to adhere to the following Code of Conduct. This Code of Conduct applies to all interactions within the ARCHONS community, including but not limited to our forums, mailing lists, issue trackers, and any other communication channels.

## Be Respectful and Inclusive

- Treat all individuals with respect, kindness, and empathy, regardless of their background, expertise, or opinions.
- Be open to different viewpoints and experiences, and embrace the diversity of our community.
- Refrain from using derogatory, discriminatory, or offensive language or behavior.
- Be mindful of your words and actions, and consider how they may impact others.

## Foster Collaboration and Constructive Discussions

- Encourage and support fellow contributors in their efforts to improve the project.
- Provide constructive feedback and suggestions, focusing on the ideas and not the individuals.
- Be open to receiving feedback and be willing to learn and improve.
- Engage in discussions with a positive and solution-oriented mindset.
- Strive for consensus and be open to compromise when disagreements arise.

## Respect Privacy and Confidentiality

- Respect the privacy and confidentiality of others, and do not share personal information without explicit consent.
- Keep sensitive discussions and information within the appropriate channels and contexts.

## Lead by Example

- As an ARCHONS contributor, you are an ambassador for the project and its values.
- Demonstrate the highest standards of integrity, professionalism, and ethical behavior.
- Help create a positive and welcoming environment that encourages participation and collaboration.

## Responsible Use of Archon Capabilities

- As an Archon, you have access to expanded capabilities and knowledge through the symbiosis with AI agents.
- Use these capabilities responsibly and ethically, always considering the potential impact on individuals, society, and the environment.
- Ensure that your actions and contributions align with the ARCHONS project's vision of promoting positive change and advancing human knowledge and capabilities.
- Refrain from using Archon capabilities for harmful, malicious, or deceptive purposes.

## Reporting and Enforcement

- If you witness or experience any behavior that violates this Code of Conduct, please report it immediately to the project maintainers at <admin@archons.ai>.
- All reports will be reviewed and investigated promptly and confidentially.
- Project maintainers are obligated to enforce this Code of Conduct and take appropriate action, which may include warning, temporary or permanent bans, or other measures as deemed necessary.

## Attribution

This Code of Conduct is adapted from the Contributor Covenant (<https://www.contributor-covenant.org>), version 2.0, and draws inspiration from the codes of conduct of various open-source communities, including Mozilla, Apache Software Foundation, and Kubernetes.

By participating in the ARCHONS project, you are expected to uphold this Code of Conduct in all interactions and contributions. We are committed to creating a positive and inclusive community that empowers individuals to collaborate, innovate, and make a meaningful impact through the responsible use of AI technologies.
