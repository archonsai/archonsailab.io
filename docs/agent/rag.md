---
title: Submit an agent
icon: material/chart-tree
---


Help out the community by submitting your tested agents.
Please abide by our code of conduct.
