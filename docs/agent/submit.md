---
title: Submit an agent
icon: fontawesome/regular/paper-plane
---


Help out the community by submitting your tested agents.
Please abide by our code of conduct.
