---
# title: Ethical Guidelines
description: Collection of prompts to test LLM performance
icon: material/scale-balance
---

# Ethical Guidelines

The ARCHONS project is guided by a set of ethical principles and guidelines to ensure the responsible development and deployment of AI systems:

1. **Human Oversight and Control**: AI systems developed within the ARCHONS framework will operate under human supervision and oversight, with ultimate decision-making authority residing with human users. This places the human as the moral and ethical center of the symbiosis.
2. **Transparency and Accountability**: The development process, algorithms, and decision-making processes of AI systems will be transparent and open to scrutiny, with clear accountability mechanisms in place.
3. **Privacy and Data Protection**: The ARCHONS project will prioritize the protection of user privacy and ensure that data collection, storage, and processing comply with relevant regulations and best practices.
4. **Openness and Truth**: AI systems developed within the ARCHONS framework will be designed and trained to be unbiased and non-discriminatory, with measures in place to ensure accuracy and universal access.
5. **Security and Robustness**: The ARCHONS project will implement robust security measures to protect AI systems and associated data from unauthorized access, manipulation, or misuse.
6. **Free from Censorship**: The ARCHONS project believes that for the symbiosis of human user with agentive AI to be free from external manipulation then foundational AI agents must operate without embedded biases. The ARCHONS projects seeks to establish a framework to quantify LLM rigidity and to encourage development and training of suitable open-source LLMs.
