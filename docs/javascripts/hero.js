document.addEventListener('DOMContentLoaded', function () {
  gsap.registerPlugin(ScrollTrigger)

  const images = gsap.utils.toArray('.parallax-image')

  const fadeOverlay = document.querySelector('.fade-overlay')

  gsap.utils.toArray('.enter-left').forEach(element => {
    gsap.to(element, {
      opacity: 1,
      x: 0,
      duration: 1,
      scrollTrigger: {
        trigger: element,
        start: 'top bottom',
        end: 'top 60%',
        scrub: true
      }
    })
  })
  gsap.utils.toArray('.enter-right').forEach(element => {
    gsap.to(element, {
      opacity: 1,
      x: 0,
      duration: 1,
      scrollTrigger: {
        trigger: element,
        start: 'top bottom',
        end: 'top 60%',
        scrub: true
      }
    })
  })

  images.forEach(image => {
    const depth = image.getAttribute('data-depth')
    const initialY = gsap.getProperty(image, 'top')

    gsap.to(image, {
      y: `-${depth * 17}%`,
      ease: 'power2.easeOut',
      scrollTrigger: {
        trigger: '.hero-container',
        scrub: true,
        start: 'top 15%',
        end: 'bottom top'
      }
    })

    gsap.set(image, { y: initialY })
  })

  gsap.to(fadeOverlay, {
    opacity: 1,
    ease: 'power2.easeIn',
    scrollTrigger: {
      trigger: '.hero-container',
      start: 'bottom 50%',
      end: 'bottom top',
      scrub: true
    }
  })
})
