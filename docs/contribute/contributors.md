---
title: Contributing to ARCHONS
icon: material/circle
---

Welcome to the ARCHONS project! We appreciate your interest in contributing to our open-source initiative. This document provides guidelines and resources to help you get started and make meaningful contributions to the project.

## Getting Started

<div class="grid cards" markdown>

- :material-clock-fast:{ .lg .middle } __Get comfortable__

    Familiarize yourself with the project's goals, philosophy, and architecture by reading the overview

    [:octicons-arrow-right-24: Vision](/concept/overview)

- :material-steering:{ .lg .middle } __Touce base__

    Join our community forums and mailing lists to engage with other contributors and stay updated on the latest developments

    [:octicons-arrow-right-24: Reference](/contribute/forum)

- :material-hammer-wrench:{ .lg .middle } __Look around__

    Explore the open issues in our issue tracker and find a task that aligns with your skills and interests. Look for issues labeled as "good first issue" or "help wanted" for beginner-friendly opportunities

    [:octicons-arrow-right-24: Testing](/test/testing)

- :material-scale-balance:{ .lg .middle } __Get tips__

    If you're new to open-source contribution, we recommend going through our contributor [Cheat Sheet](#) for helpful resources and tips.

    [:octicons-arrow-right-24: Guide](/contribute/guide)

</div>

1. Familiarize yourself with the project's goals, philosophy, and architecture by reading the documentation in the `concept/` directory.
2. Join our community forums and mailing lists to engage with other contributors and stay updated on the latest developments.
3. Explore the open issues in our issue tracker and find a task that aligns with your skills and interests. Look for issues labeled as "good first issue" or "help wanted" for beginner-friendly opportunities.
4. If you're new to open-source contribution, we recommend going through our "New Contributor Guide" and "Contributor Cheat Sheet" for helpful resources and tips.

## Contribution Process

1. Fork the ARCHONS repository to your GitHub account.
2. Create a new branch for your contribution: `git checkout -b my-contribution`.
3. Make your changes, following our coding standards and guidelines.
4. Write clear and concise commit messages for each logical change.
5. Test your changes thoroughly to ensure they don't introduce new issues.
6. Push your changes to your forked repository: `git push origin my-contribution`.
7. Open a pull request (PR) against the main ARCHONS repository, providing a detailed description of your changes and referencing any related issues.
8. Engage in the PR review process, addressing feedback and making necessary adjustments.
9. Once your PR is approved, it will be merged into the main repository.

## Coding Standards

To maintain a consistent and readable codebase, please adhere to the following coding standards:

- Follow the language-specific coding conventions and best practices.
- Use meaningful variable and function names that accurately describe their purpose.
- Write clear and concise comments to explain complex logic or algorithms.
- Maintain proper indentation and code formatting for improved readability.
- Avoid introducing new dependencies unless absolutely necessary.

## Pull Request Guidelines

When submitting a pull request, please ensure the following:

- [ ] Provide a clear and descriptive title that summarizes the purpose of your changes.
- [ ] Include a detailed description of your changes, explaining the problem you're solving or the feature you're adding.
- [ ] Reference any related issues or discussions using the appropriate keywords (e.g., "Fixes #123" or "Closes #456").
- [ ] Keep your changes focused and avoid including unrelated modifications in the same PR.
- [ ] Ensure your code is properly tested and doesn't introduce new bugs or regressions.

You will find more comprehensive [pull request guide](pr.md)

## Mentoring and Support

If you're new to open-source contribution or need guidance on a specific aspect of the project, don't hesitate to reach out to our mentors. They are experienced contributors who are willing to provide one-on-one support and help you navigate the contribution process. You can find the list of available mentors in our community forums or by reaching out to the project maintainers.

## Community Engagement

We believe that a strong and inclusive community is essential for the success of the ARCHONS project. We encourage you to actively participate in our community discussions, whether it's through our forums, mailing lists, or community meetings. Share your ideas, provide feedback, and collaborate with other contributors to shape the future of the project.

## Reporting Issues

If you encounter a bug, have a feature request, or want to discuss an improvement, please open an issue in our issue tracker. When reporting an issue, provide as much detail as possible, including:

- A clear and descriptive title
- Steps to reproduce the issue (if applicable)
- Expected behavior and actual behavior
- Relevant code snippets or screenshots
- Any additional context or information that may help in understanding and resolving the issue

## Code of Conduct

To ensure a welcoming and inclusive community, all contributors are expected to adhere to our [Code of Conduct](coc.md). Please familiarize yourself with its contents and [report any violations](conflict.md) to the project maintainers.

## Licensing

By contributing to the ARCHONS project, you agree that your contributions will be licensed under the [MIT License](LICENSE.md). Please ensure that you have the necessary rights and permissions for the code you contribute.

## Acknowledgments

We would like to express our gratitude to all the contributors who have helped shape and grow the ARCHONS project. Your dedication, expertise, and passion are invaluable to our mission of creating a powerful and collaborative open-source ecosystem.

Thank you for your interest in contributing to ARCHONS! We look forward to working with you and building an incredible community-driven project together.
