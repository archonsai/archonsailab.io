---
description: The mission objective of the Archons AI open-source projects
template: hero.html
---
<div class="enter-left" markdown>
<h1 class="call-to-action">Explore our vision of a contructive AI future</h1>
<p class="call-to-action">We believe AI future can empower human capabilities</p>
[:material-brain:](concept/overview.md){ .md-button .right }
</div>

<div class="enter-left" markdown>
<h1 class="call-to-action">Natural human interaction with AI agents</h1>
<p class="call-to-action">Interact with spoken language and get answers.</p>
<p class="call-to-action">Leverage voice-to-text and text-to-speech technologies.</p>
[:material-chat-outline:](concept/philosophy.md){ .md-button .right }
</div>

<div class="enter-left" markdown>
<h1 class="call-to-action">Build your own halo of archons</h1>
<p class="call-to-action">Self-managed and self-hosted is a core tenant.</p>
<p class="call-to-action">Archons should be extensions of you - like tools - not a subscription.</p>
[:material-graph:](archons/config.md.md){ .md-button .right }
</div>

<div class="enter-left" markdown>
<h1 class="call-to-action">Share tools and agents</h1>
<p class="call-to-action">Contribute to our open-source community </p>
<p class="call-to-action">Shared libraries tools and agent configs.</p>
[:material-hammer-wrench:](agent/submit.md){ .md-button .right }
</div>

<div class="enter-left" markdown>
<h1 class="call-to-action">Model Validation</h1>
<p class="call-to-action">We build upon elements we can trust - free from controls.</p>
<p class="call-to-action">Model capabilities and censorship systematically evaluated.</p>[:material-flask-outline:](test/testing.md){ .md-button .right }
</div>

<div class="enter-left" markdown>
<h1 class="call-to-action">Agentive AI Interface</h1>

<p class="call-to-action">It starts with humans at the heart interacting naturally with an interface layer.</p>
<p class="call-to-action">Interface layer coordinates agentive AI frameworks to work.</p>
<p class="call-to-action">All agents build around shared resources, cache, memory and tooling.</p>

```mermaid
flowchart LR 
    A{Human
    User}
    subgraph LLMS
    subgraph UI LAYER
    B((Interactive
    Agent))
    Q[(MEMORY)]
    end
    
    C(((Local
    Agent)))
    E(((Cloud
    Agent)))
    end
    
    subgraph Tools
    D([Coding Tools])
    F([Search Tools])
    G([Parsing Tools])
    H([CLI Tools])
    end
    A <==> B
    B <--> C
    B <--> E
    B o-..-o Q
    Q o-.-o C
    Q o-.-o E
    C-->D
    C-->F
    C-->G
    E-->D
    E-->F
    E-->H
```

[:material-robot-outline:](archons/install.md){ .md-button .right }
</div>
