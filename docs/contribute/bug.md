---
title: Reporting a bug
icon: material/bug-outline
---

We are thrilled to invite you to join the vibrant and passionate community of developers, researchers, and enthusiasts contributing to the ARCHONS project. This open-source initiative aims to establish a constructive symbiosis between humans and AI agents, amplifying our collective capabilities through responsible and ethical technological advancements.
